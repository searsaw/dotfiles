#!/bin/bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)

if [[ ! -f "${SCRIPT_DIR}/.secrets.env" ]]
then
  echo "copy .secrets.env.example as .secrets.env and enter the needed values"
  exit 1
fi

source "${SCRIPT_DIR}/.install.env"
source "${SCRIPT_DIR}/.secrets.env"
# shellcheck source=../.variables
source "$DOTFILES_DIR/.variables"

if [[ ! -f "$AVATAR_URL" ]]
then
  echo "there is no avatar file at '$AVATAR_URL'"
  exit 1
fi

usb_inserted=false
if [[ ! -d "$USB_PATH" ]]
then
  echo "there is not USB at path '$USB_PATH'"
else
  usb_inserted=true
fi

if ! pacman -Qi bluedevil > /dev/null 2>&1
then
  echo "installing kde plasma"
  sudo pacman -S \
    plasma kio-extras \
    plasma5-themes-breath \
    dolphin kde-utilities-meta

  # enable sddm
  systemctl enable sddm.service --force
  systemctl reboot

  # install dracula theme
  sudo pacman -S kvantum-qt5

  # configure KDE stuffs
  sudo cp "$AVATAR_URL" "/var/lib/AccountsService/icons/${USER}"
fi

if ! pacman -Qi base-devel > /dev/null 2>&1
then
  sudo pacman -S --needed base-devel
fi

if ! pacman -Qi jq > /dev/null 2>&1
then
  echo "installing cli tooling"
  sudo pacman -S bash \
    bash-completion \
    bat \
    coreutils \
    curl \
    diffutils \
    findutils \
    fzf \
    gawk \
    git \
    gnupg \
    gparted \
    grep \
    gzip \
    httpie \
    jq \
    less \
    make \
    openssh \
    prettyping \
    sed \
    shellcheck \
    tmux \
    tor \
    tree \
    unzip \
    wget \
    xclip \
    z
fi

if [[ $usb_inserted ]] && [[ ! -f "$HOME/Documents/dracula_pro.zip" ]]
then
  echo "copying and unzipping dracula pro"
  cp "$USB_PATH/dracula_pro.zip" "$HOME/Documents/dracula_pro.zip"

  (
    mkdir -p "$HOME/Documents/dracula_pro"
    cd "$HOME/Documents/dracula_pro"
    unzip ../dracula_pro.zip
  )
fi

if ! pacman -Qi vim > /dev/null 2>&1
then
  echo "installing vim"
  sudo pacman -S vim
  ln -sf "$DOTFILES_DIR/.vimrc" "$HOME/.vimrc"
fi

if ! pacman -Qi starship > /dev/null 2>&1
then
  echo "installing starship prompt"
  # config file location set using environment variable
  sudo pacman -S starship
fi

if ! pacman -Qi vim > /dev/null 2>&1
then
  echo "installing rustup"
  sudo pacman -S rustup
  rustup install stable nightly
fi

if ! pacman -Qi go > /dev/null 2>&1
then
  echo "installing go"
  sudo pacman -S go gopls delve goreleaser revive staticcheck
fi

if ! pacman -Qi lazygit > /dev/null 2>&1
then
  echo "installing lazygit"
  sudo pacman -S lazygit
fi

if ! pacman -Qi paru > /dev/null 2>&1
then
  echo "installing paru"
  (
    cd "$DOWNLOADS_DIR" || exit
    git clone https://aur.archlinux.org/paru.git
    cd paru || exit
    makepkg -si
  )
fi

if ! pacman -Qi alacritty > /dev/null 2>&1
then
  echo "installing alacritty"
  sudo pacman -S alacritty
  mkdir -p "$XDG_CONFIG_HOME/alacritty"
  ln -sf "$DOTFILES_DIR/alacritty.yml" "$XDG_CONFIG_HOME/alacritty/alacritty.yml"
fi

if ! pacman -Qi ttf-firacode-nerd > /dev/null 2>&1
then
  echo "installing fonts"
  paru -S otf-firamono-nerd ttf-firacode-nerd
  # update font cache
  fc-cache -f -v
fi

if ! pacman -Qi vscodium-bin  > /dev/null 2>&1
then
  echo "installing vscodium"
  paru -S vscodium-bin

  echo "setting up vscodium settings"
  mkdir -p "$XDG_CONFIG_HOME/VSCodium/User"
  for i in vscode/*
  do
    ln -sf "$DOTFILES_DIR/$i" "$XDG_CONFIG_HOME/VSCodium/User/$(basename "$i")"
  done

  while read -r extension
  do
    echo "installing $extension vscodium extension"
    codium --install-extension "$extension" > /dev/null
  done <vscode_extensions.txt
fi

if ! pacman -Qi discord  > /dev/null 2>&1
then
  echo "installing discord"
  sudo pacman -S discord
fi

if ! pacman -Qi bitwarden  > /dev/null 2>&1
then
  echo "installing bitwarden"
  sudo pacman -S bitwarden
fi

if ! pacman -Qi telegram-desktop  > /dev/null 2>&1
then
  echo "installing telegram"
  sudo pacman -S telegram-desktop
fi

# copy .bashrc, making sure to copy
# the initial one in the clean install
if [[ -f "$HOME/.bashrc" ]] && [[ ! -f "$HOME/.bashrc.bak" ]]
then
  echo "linking in dotfiles"
  cp "$HOME/.bashrc" "$HOME/.bashrc.bak"
  ln -sf "$DOTFILES_DIR/.bashrc" "$HOME/.bashrc"
  ln -sf "$DOTFILES_DIR/.bash_profile" "$HOME/.bash_profile"
  ln -sf "$DOTFILES_DIR/.gitconfig" "$HOME/.gitconfig"
  mkdir -p "$HOME/.local/bin" "$HOME/projects"
fi

if ! pacman -Qi tor > /dev/null 2>&1
then
  echo "installing tor and tor browser"
  sudo pacman -S tor

  # install tor browser
  (
    cd "$DOWNLOADS_DIR"
    wget "https://www.torproject.org/dist/torbrowser/$TOR_BROWSER_VERSION/tor-browser-linux64-${TOR_BROWSER_VERSION}_ALL.tar.xz"
    tar -xJf "tor-browser-linux64-${TOR_BROWSER_VERSION}_ALL.tar.xz"
    cd tor-browser
    chmod +x start-tor-browser.desktop
    ./start-tor-browser.desktop --register-app
  )
fi

if ! pacman -Qi terraform > /dev/null 2>&1
then
  echo "installing terraform"
  sudo pacman -S terraform
fi

if ! pacman -Qi signal-desktop > /dev/null 2>&1
then
  echo "installing signal"
  sudo pacman -S signal-desktop
fi

if ! pacman -Qi element-desktop > /dev/null 2>&1
then
  echo "installing element"
  sudo pacman -S element-desktop
fi

if ! pacman -Qi docker > /dev/null 2>&1
then
  echo "installing docker"
  sudo pacman -S docker docker-buildx docker-compose
  sudo usermod -a -G docker "$USER"
  sudo systemctl start docker.service
  sudo systemctl enable docker.service
fi

if [[ $usb_inserted ]] &&  [[ ! -d "$HOME/.ssh" ]]
then
  echo "setting up local ssh"
  mkdir "$HOME/.ssh"
  ssh-keygen -f "$HOME/.ssh/id_ed25519" -t ed25519 -C "$(uname -n)" -N "${SSH_PASSPHRASE}"

  for key in "$USB_PATH"/id_*
  do
    cp "$key" "$HOME/.ssh/$(basename "$key")"
  done

  chmod 700 "$HOME/.ssh"
  chmod 600 "$HOME"/.ssh/*
fi

if ! pacman -Qi tmux > /dev/null 2>&1
then
  echo "installing tmux"
  sudo pacman -S tmux
  mkdir -p "$HOME/.tmux/plugins"
  git clone https://github.com/tmux-plugins/tpm "$HOME/.tmux/plugins/tpm"
  ln -sf "$DOTFILES_DIR/.tmux.conf" "$HOME/.tmux.conf"
fi

if ! pacman -Qi mullvad-vpn > /dev/null 2>&1
then
  echo "installing mullvad client"
  paru -S mullvad-vpn
fi

if [[ $usb_inserted ]] && [[ $(gpg -K | wc -l) -eq 0 ]]
then
  echo "importing public gpg keys"
  for key in "$USB_PATH"/gpg_*.pub
  do
    gpg --import "$key"
  done

  echo "importing private gpg keys"
  for key in "$USB_PATH"/gpg_*.pvt
  do
    gpg --import "$key"
    # auto-trust imported private key
    (echo 5; echo y; echo save) | \
      gpg --command-fd 0 --no-tty --no-greeting -q --edit-key \
        "$(gpg --list-packets "$key" | awk '$1=="keyid:"{print$2;exit}')" trust
  done
fi

if ! pacman -Qi thunderbird > /dev/null 2>&1
then
  echo "installing thunderbird"
  sudo pacman -S thunderbird
fi

if [[ -d "$HOME/.local/share/applications/sparrow" ]]
then
  (
    cd "$DOWNLOADS_DIR"
    wget "https://github.com/sparrowwallet/sparrow/releases/download/$SPARROW_WALLET_VERSION/sparrow-$SPARROW_WALLET_VERSION-x86_64.tar.gz"
    wget "https://github.com/sparrowwallet/sparrow/releases/download/$SPARROW_WALLET_VERSION/sparrow-$SPARROW_WALLET_VERSION-manifest.txt"
    wget "https://github.com/sparrowwallet/sparrow/releases/download/$SPARROW_WALLET_VERSION/sparrow-$SPARROW_WALLET_VERSION-manifest.txt.asc"
    curl https://keybase.io/craigraw/pgp_keys.asc | gpg --import
    gpg --verify "sparrow-$SPARROW_WALLET_VERSION-manifest.txt.asc"
    sha256sum --check "sparrow-$SPARROW_WALLET_VERSION-manifest.txt" --ignore-missing
    tar -xzf "sparrow-$SPARROW_WALLET_VERSION-x86_64.tar.gz"
    mkdir -p "$HOME/.local/share/applications"
    mv Sparrow "$HOME/.local/share/applications/sparrow"
  )
fi
